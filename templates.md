---
layout: templates
title: Templates
permalink: /templates/
---

Use these as starting points for adopting the right Rule for your community. The templates are also available [as a book]({% link book.md %}).

