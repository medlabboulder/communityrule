---
layout: page
title: Guides
permalink: /guides/
---

These guides walk you through ways of using CommunityRule. Want a guide on another topic? Let us know.

**[Create your first Rule]({% link _guides/first_rule.md %})**

**[Module documentation]({% link _guides/modules.html %})**

**[Publish a Rule on CommunityRule]({% link _guides/publish_rule.md %})**

**[Add a Rule to a software project's Git repository]({% link _guides/git_repo.md %})**

**[Rules for mutual aid groups]({% link _guides/mutual_aid.md %})**

**[Governance workshop]({% link _guides/governance_workshop.md %})**
