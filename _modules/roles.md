---
layout: module
title: Roles
permalink: /modules/roles/
summary: Certain participants hold specialized rights and responsibilities.
type: structure
---
