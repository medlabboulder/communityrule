---
layout: module
title: Vote
permalink: /modules/vote/
type: decision
summary: Participants make a decision by voting on a proposal in some specified way.
config:
    Duration:
    Quorum:
    Threshold:
---
