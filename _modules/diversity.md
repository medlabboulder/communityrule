---
layout: module
title: Diversity
permalink: /modules/diversity/
summary: Ensure that participants are not homogeneous in certain relevant ways.
type: culture
config:
    "Relevant vectors": "age, ethnicity, gender, skills"
---
