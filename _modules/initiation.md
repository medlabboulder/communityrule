---
layout: module
title: Initiation
permalink: /modules/initiation/
summary: A procedure for how people join the community or a certain part of it.
type: process
---
