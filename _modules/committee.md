---
layout: module
title: Committee
permalink: /modules/committee/
summary: A sub-group of the community tasked with a particular role or domain of authority.
type: structure
config:
    Duration:
---
