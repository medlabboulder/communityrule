---
layout: module
title: Bureaucracy
permalink: /modules/bureaucracy/
summary: A portion of the system designed to be neutral and insulated from political pressures.
type: structure
---

Bureaucracy is a portion of a governance system designed to be neutral and insulated from political pressures. Bureaucracies tend to function on the basis of professionalism and meritocracy, rather than popular preference. They can counterbalance more transient political formations, such as elected officials and referendums.

**Input:** Externally defined priorities, meritocratic internal incentive structures, continuity through political change

**Output:** consistency of service delivery, neutral expertise in the service of popular priorities

## Background

Perhaps the most famous ancient bureaucracy was that developed in China's Han dynasty (202 BCE - 220 AD), which used expertise in Confucian philosophy as the basis for securing government positions. Other ancient governments developed bureaucratic structures as well.

Bureaucracy became a central feature of modern governments and corporations, providing operational continuity alongside policy changes from elected leaders.

## Feedback loops

### Sensitivities

* Handles matters that require expertise more effectively than elected officials or the population as a whole
* Can implement controversial policies with limited vulnerability to public pressure

### Oversights

* Can become self-perpetuating, expanding in reach to the point of subjecting political policy-making to undue constraints

## Implementations

### Communities

* Militaries under control of civilian government
* Various government ministries throughout the world

### Tools

* [Open badges](https://openbadges.org/) and other certification mechanisms
* Standardized testing

## Further resources

* Graeber, David. _The Utopia of Rules: On Technology, Stupidity, and the Secret Joys of Bureaucracy_. Melville House, 2015.
* Weber, Max. _Economy and Society_. 1922.
