---
layout: module
title: Lobbying
permalink: /modules/lobbying/
summary: People outside decision-making obtain ways to influence decision makers.
type: process
---

Lobbying refers to the practice of attempting to persuade or change the minds of officials in governance systems. It is associated with face-to-face contact and relationship-building between lobbyists and policymakers.

**Input:** Individuals willing to represent and/or advocate for a certain cause or issue with the intent of influencing those in decision-making positions, usually with in-depth understanding of federal processes and government

**Output:** Politicians, lawmakers, or decision-makers are left with information intended to persuade them to enact change to existing policy, create new policy, or block new policy

## Background

The word “lobbying” dates back to the 1640s, when lobbies of British parliament were sites of intense political debate and persuasion before and after sessions. An alternate history ascribes the word to the individuals who gathered in the lobby of the Willard Hotel in Washington, D.C. to gain access to Ulysses S. Grant, a regular there, for political favors.

In 1938, the Foreign Agents Restriction Act sought to control and track international lobbying in the U.S. It was quickly followed in 1945 when the first legislation was passed that required domestic lobbyists to register and submit reports on their activity. In 1995, the [Lobbying Disclosure Act]( https://lobbyingdisclosure.house.gov/amended_lda_guide.html) defined the legality of lobbying and the necessary practices associated with it in order to regulate it. 


## Feedback loops

### Sensitivities

* Lobbyists provide representation for citizens who may not have the time and resources to represent themselves in contesting important issues
* Allows for constant communication and visibility of issues that could easily be overshadowed by others
*Encourages participation in democracy
* The nature of lobbying as focusing on a singular issue allows for in-depth research and fact-checking by lobbyists
* Lobbyists have the opportunity to develop relationships with elected officials and achieve more impact

### Oversights

* Though intended to work on the behalf of a broadly conceived swath of citizens, lobbying for special interest groups can perpetuate the needs of a small group with certain privileges or work against the needs of the majority
* The financial aspect of lobbying influences what issues are represented, funded, and what action is taken; [wealthy interests generally control the lobbying sphere]( https://www.huffpost.com/entry/americas-lobbying-system-is-broken_b_5938a0cfe4b014ae8c69dd90?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAALn3rCa6p-rX9Vibap7AJX8PfH_b-Gcvgwyn4n8CXc1z0azuGRnmLV28SluFbzjQYubxz5WlXS3tVttP6DnJmCwh5HnQp2czC0-8xIcmDF6Pz3HSJprTDBMxfyyHYi-kzadKXX14gIAGbL26HreILkHdc0OScCpD2aVcmWvt8cJ0)
* Bribery and unethical funding of Congress members can result in complex and negative power relationships that result in undue influence 

## Implementations

### Communities

Lobbyists exist for a plethora of issues, from health care to taxation to gun laws. 

In Australia, the [Women’s Electoral Lobby]( http://welvic.org.au/) represents a variety of women’s health issues. The [Canadian Association of Petroleum Producers]( https://www.capp.ca/) represents natural gas and upstream oil issues. Lobbying in the EU is transnational and addresses a diversity of issues as well, from climate change to trade regulations.


### Tools

* [The Friends Committee on National Legislation]( https://www.fcnl.org/updates/how-to-meet-with-congress-19) provides advocacy resources
* The New Liberty Coalition created a [toolkit]( https://b.3cdn.net/noicamp4/695a1e24d0982ccb17_h3m62wee3.pdf) for lobbying for the DREAM Act – it serves as an in-depth example guide.
* Move to Amend provides a [guide]( https://movetoamend.org/sites/default/files/MTA-LobbyingGuide.pdf) to lobbying on state and national levels

## Further resources

“What is a Lobbyist?” (2018). [LobbyIt]( https://lobbyit.com/what-is-a-lobbyist/).

Heiligenstein, M. (2019). [What Is Lobbying?]( https://fitsmallbusiness.com/what-is-lobbying/).

Whiteley, P. F., & Winyard, S. J. (2018). Pressure for the poor: the poverty lobby and policy making. Routledge.

Brulle, R. J. (2018). The climate lobby: a sectoral analysis of lobbying spending on climate change in the USA, 2000 to 2016. Climatic change, 149(3-4), 289-303.


<!---cf. canvassing-->
