---
layout: module
title: Deliberation
permalink: /modules/deliberation/
summary: A space and process for discussing and reflecting on possible courses of action.
type: process
config:
    Duration:
    Facilitation:
---
