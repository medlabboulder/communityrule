---
layout: module
title: Sortition
permalink: /modules/sortition/
summary: Roles with special authority are chosen from among the community at random.
type: decision
---

Sortition is the random selection of people for positions of authority from a general pool. Those selected may serve individually or in juries and typically have the time and resources to become well informed on the questions they are chosen to decide.

**Input:** census of community members, randomization method, appropriate training

**Output:** advisory recommendation or binding decision

## Background

Sortition was a basic feature of Athenian democracy. During the medieval and early-modern period it was used for governing some Italian city-states, such as Venice and Florence.

In recent political systems, it is used most commonly for the formation of citizen juries for trials. But it has had [other scattered uses](https://en.wikipedia.org/wiki/Sortition#Modern_application) in recent history, from Amish communities to national constitutional reform processes.

## Feedback loops

### Sensitivities

* Encourages deliberation insulated from external pressures
* Can enable juries to gain a level of focused expertise on the issue at hand greater than what would be possible for the population as a whole or elected representatives

### Oversights

* May not be fully representative of the population
* May enable groupthink and negative internal dynamics among selected juries, especially in the absence of a [secret ballot](secret_ballot)

## Implementations

### Communities

* Citizen juries in judicial trials
* "[Deliberative democracy](https://scholar.google.com/scholar?q=deliberative democracy china)" processes in China
* Minds, a social network uses a [jury system for moderation](https://www.minds.com/minds/blog/power-to-the-people-the-minds-jury-system-975486713993859072)

### Tools

* [Aragon Court](https://github.com/aragon/aragon-court) - "handles subjective disputes that cannot be solved by smart contracts" using jurors incentivized to win rewards by playing a [Schelling game](https://en.wikipedia.org/wiki/Focal_point_(game_theory)
* [Random-Sample Voting](https://rsvoting.org/)
    - Implementation: [Seventh Estate Polling System](https://github.com/seventh-estate/seventh-estate)

## Further resources

* [Sortition Foundation](https://www.sortitionfoundation.org/)
* "[Sortition](https://en.wikipedia.org/wiki/Sortition)" on Wikipedia
* Van Reybrouck, David. "[Why Elections Are Bad for Democracy](https://www.theguardian.com/politics/2016/jun/29/why-elections-are-bad-for-democracy)." _The Guardian_. June 29, 2016.
