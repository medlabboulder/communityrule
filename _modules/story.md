---
layout: module
title: Story
permalink: /modules/story/
summary: A shared narrative about how or why the community became as it is.
type: culture
---