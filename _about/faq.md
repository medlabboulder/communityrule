---
layout: page
title: Frequently Asked Questions
permalink: /about/faq/
---

**Can I help contribute to improving this project?**

Absolutely, please do. The best way to start is to contribute bug reports and feature requests through the [Issues system on GitLab](https://gitlab.com/medlabboulder/communityrule/-/issues) (no programming required!). You can also submit code directly there if you're into that.

**Okay, my community has designed its Rule. What tools do we use to implement and enforce it?**

CommunityRule can't help with that. It depends a lot on what would be best for your group. If you're looking for a platform to use, [Loomio](https://loomio.org) pairs very well with a simple Rule created here. CommunityRule is also part of a broader effort, the [Metagovernance Project](https://metagov.org), which seeks to create a robust set of tools for online governance. In the meantime, good governance online tends to require some clever improvisation.

**How does CommunityRule relate to codes of conduct or license agreements?**

CommunityRule is focused on an often-missing component in online communities: the governance layer. Typically communities inherit the "[implicit feudalism](https://ntnsndr.in/implicit-feudalism)" of the underlying software, which usually centers around an all-powerful administrator. Codes of conduct (like the [Contributor Covenant](https://www.contributor-covenant.org/)), license agreements (like [Creative Commons](https://creativecommons.org/)), and other features of a community rest on that underlying governance logic. Here is one way of picturing a community "stack":

![Stack: circulation & licensing, co-creation & collaboration, policies & culture, governance & processes, people & tech]({% link assets/communitystack.png %})

The purpose of CommunityRule is to make it easier for communities to efficiently replace the default feudalism with a more appropriate and accountable governance system, or Rule. Communities can then use their preferred Rule to fill in other layers of the stack through an explicit process.

**Why does focusing on the governance layer seem important? Haven't online communities gotten along fine so far?**

This project emerged out of a series of research projects. The first is called "[Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities](https://osf.io/gxu3a/?view_only=11c9e93011df4865951f2056a64f5938)," and it traces the quiet persistence of surprisingly undemocratic tool-sets on platforms for online communities. This revealed that fostering democracy online tends to require extra work when compared to more feudal, non-democratic governance. Through the [Metagovernance Project](http://metagov.org/), this led to an ambitious proposal for software development, "[Modular Politics](https://ntnsndr.in/modular-politics)." CommunityRule is a more modest prototype, aiming to fill the gaps in online governance with a simple, natural-language toolkit in lieu of a more sophisticated software framework. 

**What's with the name "Rule"? Aren't these lists of multiple rules?**

The use of "Rule" in the singular is in part a tribute to the idiom used for the governance documents of medieval Latin monastic communities, which go by the name *Regula*. These are some of the oldest governing documents still in active use, and they serve the dual purposes of governance and spiritual guidance.

"Community Rule" is also the name of [document 1QS of the Dead Sea Scrolls](https://en.wikipedia.org/wiki/Community_Rule), which served as a guiding text for a mysterious Jewish sect from about 100 BCE. 1QS provides a vital glimpse into ancient forms of creative community-making that would be otherwise lost to time.

**What kinds of projects do you take inspiration from?**

A bunch of stuff, and we have a running list in the [project wiki](https://gitlab.com/medlabboulder/communityrule/-/wikis/Points-of-inspiration).

**What's the current project roadmap?**

A bunch of stuff, and we have a running list in the [Issues on GitLab](https://gitlab.com/medlabboulder/communityrule/-/issues).

**What projects does this draw upon?**

* Created with [Jekyll](https://jekyllrb.com/) (MIT License) and [Vue.js](https://vuejs.org/) (MIT License), hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
* Icons are from [Tabler](https://github.com/tabler/tabler-icons) (MIT License)
* Database runs on [Stein](https://steinhq.com/) (MIT License), using [this Google Doc](https://docs.google.com/spreadsheets/d/12IgsMWZNzPv2nS7ksU6CUozcS6YYwRn9O1iIXKat3V4/edit?usp=sharing)
* Modules work on mobile thanks to [dragdroptouch](https://github.com/Bernardo-Castilho/dragdroptouch) by Bernardo Castilho (MIT License)
