---
layout: vue
permalink: /templates/benevolent-dictator
rule:
  ruleID: benevolent-dictator
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_bd.svg
  name: Benevolent Dictator
  lineage: ''
  summary: >-
    The Benevolent Dictator holds ultimate decision-making power, until the group
    is ready for a more inclusive structure.
  creator:
    name: Media Economies Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Servant leadership, singular vision, voluntarism
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: >-
        Participation is open to anyone who wants to join, but the Benevolent
        Dictator can remove participants at will.
      modules: []
    - moduleID: autocracy
      name: Autocracy
      icon: null
      summary: >-
        The Benevolent Dictator has authority and can change the group's
        governance as necessary.
      modules:
        - moduleID: delegation
          name: Delegation
          icon: null
          summary: >-
            The Benevolent Dictator can invite participants to help with managing 
            the group.
          modules: []
        - moduleID: expiration
          name: Expiration
          icon: null
          summary: >-
            When the group is sufficiently mature, the Benevolent  Dictator will
            establish a more inclusive structure.
          modules: []
        - moduleID: executive
          name: Executive
          icon: null
          summary: >-
            The Benevolent Dictator is responsible for implementing—or delegating
            implementation of—policies and other decisions.
          modules: []
        - moduleID: lobbying
          name: Lobbying
          icon: null
          summary: >-
            If participants are not happy with the Benevolent Dictator's
            leadership, they may voice their concerns or leave the group.
          modules: []

---

Benevolent Dictator
===================

The Benevolent Dictator holds ultimate decision-making power, until the group is ready for a more inclusive structure.

*   **Values**: Servant leadership, singular vision, voluntarism
*   **Membership**: Participation is open to anyone who wants to join, but the Benevolent Dictator can remove participants at will.
*   **Autocracy**: The Benevolent Dictator has authority and can change the group's governance as necessary.
    *   **Delegation**: The Benevolent Dictator can invite participants to help with managing the group.
    *   **Expiration**: When the group is sufficiently mature, the Benevolent Dictator will establish a more inclusive structure.
    *   **Executive**: The Benevolent Dictator is responsible for implementing—or delegating implementation of—policies and other decisions.
    *   **Lobbying**: If participants are not happy with the Benevolent Dictator's leadership, they may voice their concerns or leave the group.

* * *

Created By
----------

Media Economies Design Lab