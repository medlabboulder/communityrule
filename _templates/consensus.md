---
layout: vue
permalink: /templates/consensus
rule:
  ruleID: consensus
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_co.svg
  name: Consensus
  lineage: ''
  summary: >-
    Decisions that affect the group collectively should involve participation of
    all participants.
  creator:
    name: Media Economies Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Creativity, empathy, solidarity
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: New participants may join as long as no others object.
      modules:
        - moduleID: exclusion
          name: Exclusion
          icon: null
          summary: >-
            Participants may be removed if a proposal to do so passes the
            consensus process.
          modules: []
    - moduleID: consensus
      name: Consensus
      icon: null
      summary: >-
        Any participant may make a proposal at any group meeting to which all
        participants are invited. A decision is made only if there are no serious
        objections remaining.
      modules:
        - moduleID: deliberation
          name: Deliberation
          icon: null
          summary: >-
            The proposal should be discussed and modified through open
            conversation in order to address all concerns. 
          modules: []
    - moduleID: do-ocracy
      name: Do-Ocracy
      icon: null
      summary: >-
        People who make proposals that the group agrees on is responsible for its
        implementation, delegating as necessary and as participants are willing to
        help.
      modules: []

---

Consensus
=========

Decisions that affect the group collectively should involve participation of all participants.

*   **Values**: Creativity, empathy, solidarity
*   **Membership**: New participants may join as long as no others object.
    *   **Exclusion**: Participants may be removed if a proposal to do so passes the consensus process.
*   **Consensus**: Any participant may make a proposal at any group meeting to which all participants are invited. A decision is made only if there are no serious objections remaining.
    *   **Deliberation**: The proposal should be discussed and modified through open conversation in order to address all concerns.
*   **Do-Ocracy**: People who make proposals that the group agrees on is responsible for its implementation, delegating as necessary and as participants are willing to help.

* * *

Created By
----------

Media Economies Design Lab