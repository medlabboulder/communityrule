---
layout: vue
permalink: /templates/petition
rule:
  ruleID: petition
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_pe.svg
  name: Petition
  lineage: ''
  summary: All participants can propose and vote on proposals for the group.
  creator:
    name: Media Economies Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Equality, initiative, majority rule
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: Petitions set policies for membership and removal.
      modules: []
    - moduleID: petition
      name: Petition
      icon: null
      summary: >-
        Any structures must be established by petition. All participants have the
        right to initiate proposals, sign them, and vote in resulting referendums.
        If a certain percentage of participants signs a proposal, it goes to a
        referendum.
      modules: []

---

Petition
========

All participants can propose and vote on proposals for the group.

*   **Values**: Equality, initiative, majority rule
*   **Membership**: Petitions set policies for membership and removal.
*   **Petition**: Any structures must be established by petition. All participants have the right to initiate proposals, sign them, and vote in resulting referendums. If a certain percentage of participants signs a proposal, it goes to a referendum.

* * *

Created By
----------

Media Economies Design Lab