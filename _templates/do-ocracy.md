---
layout: vue
permalink: /templates/do-ocracy
rule:
  ruleID: do-ocracy
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_do.svg
  name: Do-ocracy
  lineage: ''
  summary: >-
    Those who take initiative to do something in the group can decide how they do
    it.
  creator:
    name: Media Economies Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Bias for action, consultation, decentralization
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: >-
        Anyone who actively contributes to the community's mission can
        self-identify as a participant.
      modules: []
    - moduleID: autonomy
      name: Autonomy
      icon: null
      summary: >-
        There are no fixed structures. Participants can organize meetings or
        sub-groups as they see fit.
      modules: []
    - moduleID: do-ocracy
      name: Do-ocracy
      icon: null
      summary: >-
        Participants are responsible for carrying out their own initiatives,
        optionally in collaboration with others.
      modules:
        - moduleID: lobbying
          name: Lobbying
          icon: null
          summary: >-
            If someone has serious concerns about an initiative, they should raise
            those concerns with the person responsible for it.
          modules: []

---

Do-ocracy
=========

Those who take initiative to do something in the group can decide how they do it.

*   **Values**: Bias for action, consultation, decentralization
*   **Membership**: Anyone who actively contributes to the community's mission can self-identify as a participant.
*   **Autonomy**: There are no fixed structures. Participants can organize meetings or sub-groups as they see fit.
*   **Do-ocracy**: Participants are responsible for carrying out their own initiatives, optionally in collaboration with others.
    *   **Lobbying**: If someone has serious concerns about an initiative, they should raise those concerns with the person responsible for it.

* * *

Created By
----------

Media Economies Design Lab