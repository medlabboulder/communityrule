# CommunityRule [inactive repository]

**The repos for this project are now located [here](https://git.medlab.host/CommunityRule)**

---

A governance toolkit for great communities, located at [communityrule.info](https://communityrule.info).

---

This project welcomes contributors. All contributions are assumed to accept the project's GPL and Creative Commons licenses.

To contribute governance templates, copy an existing one at `_template/[template_name].md` and fill out the YAML metadata. This will automatically add a new template into the system. Propose edits to existing governance templates at `_template/[template_name].md`.

The app currently depends on Vue.js. Major scripts are located at `assets/js/`.

To get involved, [contact MEDLab](mailto:medlab@colorado.edu).

# Run Locally

1. [Install Jekyll Prerequisites](https://jekyllrb.com/docs/installation)
2. Install Jekyll and the bundler with `gem install jekyll bundler`
3. Install project dependencies with `bundle install`
4. Run the webserver with `bundle exec jekyll serve`
5. View your site locally at [https://localhost:4000]](https://localhost:4000)
